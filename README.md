**This is a technical assessment that should last for an hour(60 min)**

---
## Introduction
This project has been set up to use maven and some dependencies have been provided.
You may change the dependencies only when you necessary for example you may want the use the older version of a dependency if you are not familiar with the new one

---
## Task
User should be able to buy an item form any online shopping site and validate that the correct item is added to the cart.

---
## Objectives
- Create feature file using Gherkin syntax
- Create step definitions and link these to the feature file
- Create TestRunner to run test and links both pieces up (step definition and feature)
- Validate that the correct item is added to the cart
- Make sure the test passes

---
## Notes
- The use of Page Object Model will be considered but not a deal breaker for this test
- Please demonstrate in all forms your solution to the task as not coding style is bad code 

---
## Credentials

* username: sanitymatch
* Password: Pa55word@123

All the best!